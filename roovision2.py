''' RooVision for Ian Wood on behalf of REMAP 
	MK2 Headset
	This project has a Git repository here:
	https://gitlab.com/Remap_Charity/roovision

'''
# import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
from imutils.video.pivideostream import PiVideoStream
import imutils
import time
import cv2
import numpy as np
import pygame
from subprocess import call
import RPi.GPIO as GPIO  

__version__ = "v2.4"

FULL_SCREEN = True

borderWidth = 50	#Default to 50 but can be tweaked
imageWidth = 1024	#512
imageHeight = 600	#608
halfWidth = int(imageWidth / 2)
halfHeight = int(imageHeight / 2)

videoMode = {'freeze':False, 'neg':False, 'edge':False, 'zoom':False, 'cal':False }
 
pygame.init()
pygame.joystick.init()

# Use the Broadcom SOC Pin numbers 
# Setup the Pin with Internal pullups enabled and PIN in reading mode. 
GPIO.setmode(GPIO.BCM)  
GPIO.setup(18, GPIO.IN, pull_up_down = GPIO.PUD_UP)  

# Our function on what to do when the button is pressed 
def Shutdown(channel):  
   os.system("sudo shutdown -h now")  

GPIO.add_event_detect(18, GPIO.FALLING, callback = Shutdown, bouncetime = 2000)  

def strokeWatchdog():
	''' This starts the watchdog going and keeps it going '''
	f = open("/dev/watchdog", "w")
	f.write("S")
	f.close
	
def stopWatchdog():
	''' this stops the watchdog rebooting the system '''
	f = open("/dev/watchdog", "w")
	f.write("V")
	f.close
	print("Watchdog stopped")

windowName = "RooVision"
font = cv2.FONT_HERSHEY_SIMPLEX
batteryWarning = False
text = "CAL < >"

# created a *threaded *video stream, allow the camera sensor to warmup,
vs = PiVideoStream(resolution=(imageWidth, imageHeight), framerate=30).start()
time.sleep(1.0)

''' Joystic controls - only if you find a joystick should you use it...'''
joystick = None
try:
	if pygame.joystick.get_count(): # test to see if the gamepad is connected
		joystick = pygame.joystick.Joystick(0)
		joystick.init()
	else:
		print('No game controllers found')
except Exception as identifier:
	print('Starting without game controller')

# capture frames from the camera
#for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True)
while True:
	''' Do this forever'''
	frame = vs.read()
	if joystick:
		for event in pygame.event.get():
			if event.type == pygame.JOYBUTTONDOWN:
				for i in range(5):
					button = joystick.get_button( i )
					#print('But {} = {}'.format(i, button))
					if button == 1:
						if i == 1:
							videoMode['freeze'] = not videoMode['freeze']
						elif i == 2:
							videoMode['edge'] = not videoMode['edge']
						elif i == 3:
							videoMode['neg'] = not videoMode['neg']
						elif i == 0:
							videoMode['zoom'] = not videoMode['zoom']
						elif i == 4:
							videoMode['cal'] = not videoMode['cal']
							batteryWarning = not batteryWarning
					#print(videoMode)
	
	''' Video preccessing '''
	if not videoMode['freeze']:
		rawImage = frame
	if videoMode['zoom']:
		image = rawImage[202:405, 170:341]
	else:
		image = rawImage
	if videoMode['neg']:
		image = cv2.bitwise_not(image)
	if videoMode['edge']:
		image = cv2.Canny(image,100,200)
	if videoMode['cal']:
		# Draw a black screen with crosshairs
		#image = np.zeros((imageHeight, imageWidth,3), np.uint8)
		cv2.line(image,(halfWidth,0),(halfWidth, imageHeight),(0,255,0),1) 
		cv2.line(image,(0,halfHeight),(imageWidth, halfHeight),(0,255,0),1)
		hat = joystick.get_hat(0)
		#print(hat[0])
		global borderWidth
		global text
		if hat[0] == 1:
			if borderWidth < imageWidth:
				borderWidth += 1
				text = str(borderWidth)
		elif hat[0] == -1:
			if borderWidth > 0:
				borderWidth -= 1
				text = str(borderWidth)
	
	''' Uncoment the following line to activate the watchdog 
		to stop the watchdog press "s" on the keyboard (or comment this out)'''
	#strokeWatchdog()
	# Overlay battery warning if set
	if batteryWarning:
		cv2.putText(image, text, (105, 200), font,3,(0,255,0), 3, cv2.LINE_AA)
	# Make left & right copies for each eyes
	left = cv2.copyMakeBorder(image,0,0,borderWidth,0,cv2.BORDER_CONSTANT,value=[0, 0, 0])
	right = cv2.copyMakeBorder(image,0,0,0,borderWidth,cv2.BORDER_CONSTANT,value=[0, 0, 0])
	# Stack them side by side
	both = np.hstack((left, right))
	cv2.namedWindow(windowName, cv2.WINDOW_NORMAL)
	# Make the window match the LCD resolution
	cv2.resizeWindow(windowName, 1024, 600)
	# Make the window full-Screen
	if FULL_SCREEN: cv2.setWindowProperty(windowName, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
	# Display the stereo image
	cv2.imshow(windowName, both)
	# Get keyboard input
	key = cv2.waitKey(1) & 0xFF
 
	# clear the stream in preparation for the next frame
	#rawCapture.truncate(0)
 
	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		# Stop the capture thread
		vs.stop()
		break
	elif key == ord("b"):
		batteryWarning = not batteryWarning
	elif key == ord("s"):
		stopWatchdog()
		
