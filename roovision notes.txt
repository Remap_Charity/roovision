*******************************************
RooVision Notes relating to project
*******************************************

WATCHDOG

I had this problem when i worked with Raspberry Pi. My application used extensive CPU time. After 1 or 2 days work it caused the Raspberry pi hangs. So i decided to use the wtchdog. When i wanted to write watchdog device from C++ program i got same error.

The solution that i found:

open a new rule file sudo nano /etc/udev/rules.d/60-watchdog.rules

and add this line to the file KERNEL=="watchdog", MODE="0666"

After this, i was able to access watchdog form terminal screen or c++ program.

RGP - This works fine!

TO DO
------

1) Lock-ring for lens DONE
2) Invert Camera DONE
3) Invert LCD NO NEED
4) Re-do battery holder DONE
5) incorporate on/off switch NOT NEEDED
6) fit UPS board DONE
7) Integrate game controller DONE
8) 

image = rawImage[170:300, 400:500]
				[ startY:endY, startX:endX ]

Splash Screen is in:
/usr/share/plymouth/themes/pix
Edit it in pix.script

*** DON'T FORGET TO PUSH CHANGES TO GITLAB ***
https://gitlab.com/Remap_Charity/roovision.git