# RooVision

A vision enhancement system for the partially sighted.

For a full description of this award winning project see here:
http://www.remap.org.uk/2125/vision-system/

I am still working on uploading all the 3D STL files. This will be done soon.

I am working on a simpler MK3 version which will be less expensive and hopefully even simpler to use.